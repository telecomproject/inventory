Руководство пользователя 
==================

.. toctree::
   :maxdepth: 2
   :caption: Общая информация

   terms-definitions
   introduction
   terms-of-use
   getting-started
   trial


.. toctree::
   :maxdepth: 2
   :caption: Работа с расширением

   ext-install-setup
   ext-review
   ext-mob-device
   ext-add-info
   ext-sync-mob-device


.. toctree::
   :maxdepth: 2
   :caption: Работа с мобильным приложением

   app-install-setup
   app-review
   app-view-inventory
   app-collect-info
   app-sync-info

.. toctree::
   :maxdepth: 2
   :caption: FAQ

   cert
   using-map
   update-app-ext
   kadastr-map

